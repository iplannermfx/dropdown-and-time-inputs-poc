import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { pipe } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss']
})
export class DateInputComponent implements OnInit {
  @Input() control: FormControl;
  auxControl = new FormControl('', );
  constructor() { }

  ngOnInit(): void {
    this.auxControl.valueChanges.
      pipe(
        distinctUntilChanged(),
        debounceTime(200),
        filter(value => !!value)
      )
      .subscribe(input => {
        const {value} = this.control;
        const hours = moment(value).hours();
        const minutes = moment(value).minutes();
        this.control.setValue(moment(`${input} ${hours}:${minutes}`).format());
      });
  }

}
