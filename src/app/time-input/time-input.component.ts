import { distinctUntilChanged, debounceTime, filter } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-time-input',
  templateUrl: './time-input.component.html',
  styleUrls: ['./time-input.component.scss']
})
export class TimeInputComponent implements OnInit {
  @Input() control: FormControl;
  auxControl = new FormControl('', );

  constructor() { }

  ngOnInit(): void {
    this.auxControl.valueChanges.
        pipe(
          distinctUntilChanged(),
          debounceTime(200),
          filter(value => !!value)
        )
        .subscribe(input => {
          const [hour, minute] = input.split(':');
          const { value } = this.control;
          let currentDate = value;
          const prefix = moment(value).format('YYYY-MM-DD');
          currentDate = moment(prefix + ' ' + input);
          console.log('🚀', currentDate);
          this.control.setValue(moment(currentDate).format());
        });
  }
}
